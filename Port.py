import socket


class Port: 


	def __init__(self):
		pass


	@classmethod
	def Check(cls, ip, port, time=0.0001):

		""" Checks if it is a port is open or not."""

		lock = 0


		if isinstance(ip, str):
			lock += 1


		if isinstance(port, int):
			if port in range(1, 65537):
				lock += 1


		if isinstance(time, float):
			if time > 0 and time < 1:
				lock += 1


		if lock == 3:

			try:
				with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
					s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
					s.settimeout(time)
					s.connect((ip, port))
					s.shutdown(socket.SHUT_RDWR)

				return True

			except socket.error as _:
				return False