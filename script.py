import os
import sys
from Port import Port

class App:


	def __init__(self):
		pass


	@classmethod
	def way(cls):

		""" Displays whether a particular port is open, closed or filtered. """

		# Returns False if it is closed or filtered
		# Returns True if open

		print(["Close | Filtered", "Open"][int(Port.Check("192.168.1.1", 53))])



	@classmethod
	def way2(cls):

		""" Scan base ports. """

		for i in range(1, 1025):
			if Port.Check("192.168.1.1", i):
				sys.stdout.write("{}\n".format(i))


	@classmethod
	def way3(cls):

		""" Scan all ports. """

		for i in range(1, 65536):
			if Port.Check("192.168.1.1", i):
				sys.stdout.write("{}\n".format(i))


	@classmethod
	def way4(cls):

		ip = "192.168.1.1"

		for i in range(1, 1025):

			sys.stdout.write("Analysis Port : {:5}".format(i))

			if Port.Check(ip, i):
				sys.stdout.write(f"\t[+] Open\t\t# {ip}\n")
			else:
				sys.stdout.write(f"\t[-] Closed | Filtered\t# {ip}\n")

App.way()
#App.way2()
#App.way3()
#App.way4()


if sys.platform.startswith("win32"):
	os.system("pause")
