<h1>Port Scanner</h1>
<h3>What is a port scanner?</h3>
<ul>
    <li>https://en.wikipedia.org/wiki/Port_scanner</li>
</ul>

<h2>Script</h2>
<h3><b>Windows</b></h3> 
<p>Double click on the "script.py" file</p>
  
<h3><b>Linux & Mac</b></h3>
<p>Modify the execution rights then execute the script</p>
<pre>
    chmod +x script.py
    ./script.py
</pre>

<h2>Contact me</h2>
<p>Feel free to contact me if you have any questions, criticisms, suggestions, bugs encountered </p>

<p><b>Mail :</b> mx.moreau1@gmail.com<p>
<p><b>Twitter :</b> mxmmoreau (https://twitter.com/mxmmoreau)</p>